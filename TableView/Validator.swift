//
//  Validator.swift
//  Utilities
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation

public class Validator {
    
    private var evaluators = [RuleEvaluator]()
    private var errors = [String]()
    
    public init() {
        
    }
    
    public func  validateNumbers(_ numbers: [NSNumber?]) -> RuleAdder<NumberRule> {
        let numberRuleEvaluator = Evaluator<NumberRule>.init(values: numbers)
        evaluators.append(numberRuleEvaluator)
        
        return RuleAdder<NumberRule>.init(evaluator: numberRuleEvaluator)
    }
    
    public func  validateNumber(_ number: NSNumber?) -> RuleAdder<NumberRule> {
        let numberRuleEvaluator = Evaluator<NumberRule>.init(value: number)
        evaluators.append(numberRuleEvaluator)
        
        return RuleAdder<NumberRule>.init(evaluator: numberRuleEvaluator)
    }
    
    public func  validateStrings(_ values: [String?]) -> RuleAdder<TextRule> {
        let textRuleEvaluator = Evaluator<TextRule>.init(values: values)
        evaluators.append(textRuleEvaluator)
        
        return RuleAdder<TextRule>.init(evaluator: textRuleEvaluator)
    }
    
    public func  validateString(_ value: String?) -> RuleAdder<TextRule> {
        let textRuleEvaluator = Evaluator<TextRule>.init(value: value)
        evaluators.append(textRuleEvaluator)
        
        return RuleAdder<TextRule>.init(evaluator: textRuleEvaluator)
    }
    
    public func isDataValid() -> Bool {
        errors.removeAll()
        for evaluator in evaluators {
            processEvaluator(evaluator: evaluator)
        }
        
        if errors.count > 0 {
            return false
        } else {
            return true
        }
    }
    
    private func processEvaluator(evaluator: RuleEvaluator) {
        do {
            try evaluator.evaluate()
        } catch EvaluationError.failedWithErrors(let errorMessages) {
            errors.append(contentsOf: errorMessages)
        } catch {
            errors.append("unknown error")
        }
    }
    
}
