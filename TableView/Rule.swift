import Foundation

enum EvaluationError: Error {
    case failed(String)
    case failedWithErrors(errors: [String])
}

public protocol Rule {
    associatedtype T
    func evaluate(value: T) throws
}
