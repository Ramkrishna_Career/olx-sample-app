import Foundation

class RuleEvaluator {
    func evaluate() throws {
        preconditionFailure("not implemented")
    }
}

class Evaluator<R: Rule>: RuleEvaluator {
    
    private var rules = [R]()
    private var valuesToEvaluate = Array<R.T>()
    private var errors = [String]()
    
    init (value: R.T) {
        valuesToEvaluate.append(value)
    }
    
    init(values: [R.T]) {
        valuesToEvaluate.append(contentsOf: values)
    }
    
    func addRule(rule: R) {
        rules.append(rule)
    }
    
    func addRules(rules: [R]) {
        self.rules.append(contentsOf: rules)
    }
    
    override func evaluate() throws {
        errors.removeAll()
        for rule in rules {
            evaluateRule(rule: rule, forValues: valuesToEvaluate)
        }
        
        if errors.count > 0 {
            throw EvaluationError.failedWithErrors(errors: errors)
        }
    }
    
    private func evaluateRule(rule: R, forValues values: [R.T]) {
        for value in values {
            evaluateRule(rule: rule, forValue: value)
        }
    }
    
    private func evaluateRule(rule: R, forValue value: R.T) {
        do {
            try rule.evaluate(value: value)
        } catch EvaluationError.failed(let message) {
            errors.append(message)
        } catch {
            errors.append("unknown error")
        }
    }
}
