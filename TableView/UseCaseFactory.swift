//
//  UseCaseFactory.swift
//  
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//
import Foundation

class UseCaseFactory {
    
    static func makeItemSearch() -> SearchItem {
        //let dataSource =
        let searchItem = SearchItem.init(dataSource: RemoteDataFetcher())
        return searchItem
    }
    
    static func loadMoreItems() -> LoadMoreItem {
        //let dataSource =
        let loadMoreItem = LoadMoreItem.init(dataSource: RemoteDataFetcher())
        return loadMoreItem
    }
    
    static func saveNewItems() -> saveNewData {
        let newData = saveNewData.init(dataSource: LocalDataFetcher())
        return newData
    }
    
    static func getSavedItems() -> LoadSavedItem {
        let savedData = LoadSavedItem.init(dataSource: LocalDataFetcher())
        return savedData
    }
    
}
