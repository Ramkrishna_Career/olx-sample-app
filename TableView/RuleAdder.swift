//
//  RuleAdder.swift
//  Utilities
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation

public class RuleAdder<R: Rule> {
    
    private var evaluator: Evaluator<R>!
    
    init(evaluator: Evaluator<R>) {
        self.evaluator = evaluator
    }
    
    public func addRule(_ rule: R) {
        evaluator.addRule(rule: rule)
    }
    
    public func addRules(_ rules: [R]) {
        evaluator.addRules(rules: rules)
    }
    
}
