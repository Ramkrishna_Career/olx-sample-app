//
//  ItemServices.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//


import DATAStack
import Foundation
import Alamofire

enum BackendError: Error {
  case urlError(reason: String)
  case objectSerialization(reason: String)
}

enum ItemFields {
  
    enum Item : String {
        case ID = "id"
        case Title = "title"
        case Description = "description"
        case MediumImage = "mediumImage"
        case DisplayLocation = "displayLocation"
        case Price = "price"
    
    }
    
    enum Price : String {
        case Amount = "amount"
        case DisplayPrice = "displayPrice"
        case PreCurrency = "preCurrency"
        case PostCurrency = "postCurrency"
    }
    
    enum DateField : String {
        case Timestamp = "timestamp"
        
        // TO DO
        
    }
  
    
}

 class ItemWrapper {
  var items: [ItemServices]?
  var count: Int?
  var next: String?
  var previous: String?
    
}

class ItemServices {
  var idNumber: Int64?
  var title: String?
  var itemDescription: String?
  var mediumImage: String?
  var displayLocation: String?
  var price : Price?
    var date : DateValue?
    
    class Price {
        var amount: Int?
        var displayPrice : String?
        var preCurrency: String?
        var postCurrency: String?
    
    }
    class DateValue {
        var timeStamp : String?
    }
    
    required init(json: [String: Any]) {
     
    self.idNumber = json[ItemFields.Item.ID.rawValue] as? Int64
    self.title = json[ItemFields.Item.Title.rawValue] as? String
    self.itemDescription = json[ItemFields.Item.Description.rawValue] as? String
    self.displayLocation = json[ItemFields.Item.DisplayLocation.rawValue] as? String
    self.mediumImage =  json[ItemFields.Item.MediumImage.rawValue] as? String
    
    
        let dateJson = json["date"] as? [String : Any]
        self.date = DateValue()
        self.date?.timeStamp  = dateJson? [ItemFields.DateField.Timestamp.rawValue] as? String
        
        if let priceJson = json["price"] as? [String : Any] {
            self.price = Price()
            self.price?.amount = priceJson[ItemFields.Price.Amount.rawValue] as? Int
            
            if let displayPrice =  priceJson[ItemFields.Price.DisplayPrice.rawValue] as? String {
                self.price?.displayPrice = displayPrice
            }
            else{
                self.price?.displayPrice = ""
            }
        }
        else{
            self.price = Price()
            self.price?.displayPrice = ""
        }
    // TODO: more fields!
  }
    
  
  // MARK: Endpoints
  
    class func endpointForSearchItem(_ searchItem : String) -> String {
        
        let url = "http://api-v2.olx.com/items?location=www.olx.com.ar&searchTerm=\(searchItem)"
        let escapedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return escapedUrl!
  }
  
   

  // GET / Read all items
  fileprivate class func getitemsAtPath(_ path: String, completionHandler: @escaping (Result<ItemWrapper>) -> Void) {
    // make sure it's HTTPS because sometimes the API gives us HTTP URLs
    guard var urlComponents = URLComponents(string: path) else {
      let error = BackendError.urlError(reason: "Tried to load an invalid URL")
      completionHandler(.failure(error))
      return
    }
    urlComponents.scheme = "https"
    guard let url = try? urlComponents.asURL() else {
      let error = BackendError.urlError(reason: "Tried to load an invalid URL")
      completionHandler(.failure(error))
      return
    }
    let _ = Alamofire.request(url)
      .responseJSON { response in
        if let error = response.result.error {
          completionHandler(.failure(error))
          return
        }
        let ItemWrapperResult = ItemServices.itemsArrayFromResponse(response)
        completionHandler(ItemWrapperResult)
    }
  }
  
   class func getitems(_ item : String, _ completionHandler: @escaping (Result<ItemWrapper>) -> Void) {
    getitemsAtPath(ItemServices.endpointForSearchItem(item), completionHandler: completionHandler)
  }
  
  class func getMoreitems(_ wrapper: ItemWrapper?, completionHandler: @escaping (Result<ItemWrapper>) -> Void) {
    guard let nextURL = wrapper?.next else {
      let error = BackendError.objectSerialization(reason: "Did not get wrapper for more items")
      completionHandler(.failure(error))
      return
    }
    getitemsAtPath("http://api-v2.olx.com"+nextURL, completionHandler: completionHandler)
  }
  
  fileprivate class func itemsFromResponse(_ response: DataResponse<Any>) -> Result<ItemServices> {
    guard response.result.error == nil else {
      // got an error in getting the data, need to handle it
      print(response.result.error!)
      return .failure(response.result.error!)
    }
    
    // make sure we got JSON and it's a dictionary
    guard let json = response.result.value as? [String: Any] else {
      print("didn't get items object as JSON from API")
      return .failure(BackendError.objectSerialization(reason:
        "Did not get JSON dictionary in response"))
    }
    
    let items = ItemServices(json: json)
    return .success(items)
  }
  
  fileprivate class func itemsArrayFromResponse(_ response: DataResponse<Any>) -> Result<ItemWrapper> {
    guard response.result.error == nil else {
      // got an error in getting the data, need to handle it
      print(response.result.error!)
      return .failure(response.result.error!)
    }
    
    // make sure we got JSON and it's a dictionary
    guard let json = response.result.value as? [String: Any] else {
      print("didn't get items object as JSON from API")
      return .failure(BackendError.objectSerialization(reason:
        "Did not get JSON dictionary in response"))
    }
    
   
    let wrapper:ItemWrapper = ItemWrapper()
    
    let metadata = json["metadata"] as! [String: Any]
    
    wrapper.next = metadata["next"] as? String
    wrapper.previous = metadata["previous"] as? String
    wrapper.count = metadata["total"] as? Int
    
    

    var allitems: [ItemServices] = []
    if let results = json["data"] as? [[String: Any]] {
      for jsonitems in results {
        let items = ItemServices(json: jsonitems)
        allitems.append(items)
      }
    }
    wrapper.items = allitems
    return .success(wrapper)
  }
    
   
    
   
    
}








