//
//  AppTableViewCell.swift
//  TableView
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import UIKit
import SDWebImage
class AppTableViewCell: UITableViewCell
{
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var backgroundCardView: UIView!
      @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var locationLabel: UILabel!
   

    @IBOutlet weak var postedOn: UILabel!
    
    func updateUI(spices : ItemServices) {
   
        titleLabel.text = spices.title
        descriptionLabel.text = spices.itemDescription
    
        
        if let dateString = spices.date?.timeStamp! {
        createdAtLabel
            .text =    self.convertDateFormatter(date:dateString)
            
        }
        
        
        
        
        
            if let displayPrice = spices.price?.displayPrice{
                   locationLabel.text = spices.displayLocation! + " | \(displayPrice)"
            }else{
                locationLabel.text = spices.displayLocation!
        }
        
     
        ImageLibraryManager.sharedInstance.downloadImage(imageURL: NSURL(string: spices.mediumImage!) as URL!, success: { (downloadedImage) in
                self.thumbnailImageView.image = downloadedImage
                self.activityIndicator.stopAnimating()
            }
            ,  failure: { (errorMessage) in
                self.thumbnailImageView.image = UIImage.init(named: "placeholder")
                self.activityIndicator.stopAnimating()
        })
        
        thumbnailImageView.layer.shadowColor = UIColor.black.cgColor
        thumbnailImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        thumbnailImageView.layer.shadowRadius = 6
        thumbnailImageView.layer.shadowOpacity = 1
        
       
    
    
    backgroundCardView.backgroundColor = UIColor.white
    contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
    
    backgroundCardView.layer.cornerRadius = 3.0
    backgroundCardView.layer.masksToBounds = false
    
    backgroundCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
    
    backgroundCardView.layer.shadowOffset = CGSize(width: 0, height: 0)
    backgroundCardView.layer.shadowOpacity = 0.8
    
    }
    
    
    
    func convertDateFormatter(date: String) -> String
    {
        
     
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: date)
        
        
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: date!)
        
        
        return timeStamp
    }
    
}
























