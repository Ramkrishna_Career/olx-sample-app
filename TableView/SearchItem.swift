//
//  SearchItem.swift
//  
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//

import Foundation


struct SearchItemRequest : RequestBase {
    
    var searchItem: String?
    var itemWrapper : ItemWrapper?
    var item : [ItemServices]?
    
}

struct SearchItemResponse : ResponseBase {
    var items : ItemWrapper?
    var saveItem : Bool?
    var item : [ItemServices]?
    
}

class SearchItem: UseCase<SearchItemRequest , SearchItemResponse> {
    private var dataSource: ItemDataSource
    
    init(dataSource:ItemDataSource) {
        self.dataSource = dataSource
    }
    
    override func execute(_ request: SearchItemRequest) {
        
        if isRequestValid(request) {
            let dataSource = RemoteDataFetcher()
          
            dataSource.getItem(searchItem: request.searchItem!, onResponse: { (response) in
                let itemWrapper = SearchItemResponse.init(items: response,saveItem:nil,item : nil)
                self.getCallback()?.success(response:itemWrapper)
                

            }, onFailure: { (error) in
                self.getCallback()?.failure(error: error)
            })
        
        }
    }

    private func isRequestValid(_ request: SearchItemRequest) -> Bool {
        
        let validator = Validator()
        validator.validateString(request.searchItem).addRule(NotEmpty())
        return validator.isDataValid()
    }
    
}

class LoadMoreItem :UseCase<SearchItemRequest , SearchItemResponse>{
    
    private var dataSource: ItemDataSource
    init(dataSource:ItemDataSource) {
        self.dataSource = dataSource
    }
    
    override func execute(_ request: SearchItemRequest) {
        
            if isRequestNextURLValid(request) {
                let dataSource = RemoteDataFetcher()
                dataSource.getMoreItem(itemWrapper: request.itemWrapper!, onResponse: { (response) in
                    let itemWrapper = SearchItemResponse.init(items: response,saveItem:nil ,item : nil)
                    self.getCallback()?.success(response:itemWrapper)
                    
                }, onFailure: { (error) in
                    
                    self.getCallback()?.failure(error: error)
                    
                })
                
            }
            
        }
    
    
    private func isRequestNextURLValid(_ request: SearchItemRequest) -> Bool {
        let validator = Validator()
        validator.validateString(request.itemWrapper?.next).addRule(NotEmpty())
        return validator.isDataValid()
    }

}

class saveNewData: UseCase<SearchItemRequest , SearchItemResponse>{
    private var dataSource: ItemDataSource
   
    init(dataSource:ItemDataSource) {
        self.dataSource = dataSource
    }
    
    override func execute(_ request: SearchItemRequest) {
         if isRequestItemWrapperValid(request) {
            let localDataSource = LocalDataFetcher()
            let json = convertToJson(request)
                   
            localDataSource.saveNewItem(items: json , onResponse: { (result) in
                
            }, onFailure: { (error) in
                
            })
        }
        
    }
    
    private func convertToJson(_ request: SearchItemRequest) -> [[String : Any]]{
        
        var jsonArray : [[String : Any]]? = []
        for item in (request.item)! {
          
            let json: [String : Any]  = [
        
                    "id": item.idNumber! as Int64 ,
                    "title": item.title! as String,
                    "description": item.itemDescription! as String,
                    "mediumImage": item.mediumImage! as String,
                    "displayLocation": item.displayLocation! as String,
                    "displayPrice":(item.price!.displayPrice)! as String,
                    "timestamp":(item.date!.timeStamp)! as String,
                
            ]
            jsonArray?.append(json)
        }
    
        return jsonArray!
        
    }
    
    private func isRequestItemWrapperValid(_ request: SearchItemRequest) -> Bool {
        let validator = Validator()
        
        for item in request.item! {
            validator.validateStrings([item.title]).addRule(NotEmpty())
        }
        return validator.isDataValid()
    }
    
    
}

class LoadSavedItem :UseCase<SearchItemRequest , SearchItemResponse>{
    
    private var dataSource: ItemDataSource
    init(dataSource:ItemDataSource) {
        self.dataSource = dataSource
    }
    
    override func execute(_ request: SearchItemRequest) {
        
        
        let dataSource = LocalDataFetcher()
        dataSource.getSavedItems(onResponse: { items in
            let savedItems = SearchItemResponse.init(items: nil,saveItem:nil ,item :items )
            
            self.getCallback()?.success(response: savedItems)
        }, onFailure: { (error) in
            self.getCallback()?.failure(error: error)
        })
        
        
        
        
    }
    
    
    private func isRequestNextURLValid(_ request: SearchItemRequest) -> Bool {
        let validator = Validator()
        validator.validateString(request.itemWrapper?.next).addRule(NotEmpty())
        return validator.isDataValid()
    }
    
}
