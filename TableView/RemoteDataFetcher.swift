//
//  RemoteDataFetcher.swift
//
//  Created by Ramkrishna Baddi on 27/02/17.
//  Copyright © 2017 Krishna. All rights reserved.
//
import Foundation
import DATAStack
class RemoteDataFetcher: ItemDataSource  {

    func getItem(searchItem: String, onResponse: @escaping (ItemWrapper) -> Void, onFailure: @escaping (String) -> Void) {
        
        ItemServices.getitems(searchItem) { result in
            if let error = result.error {
                // TODO: improved error handling
                 onFailure(error.localizedDescription)
            }
            else{
                onResponse(result.value!)
            }
        }
    }
    
    func saveNewItem(items: [[String: Any]], onResponse: (Bool) -> Void, onFailure: (String) -> Void) {
        
    }
    
     func getSavedItems(onResponse: @escaping ([ItemServices]) -> Void, onFailure: @escaping (String) -> Void) {
        
    }
    
    func getMoreItem(itemWrapper: ItemWrapper,onResponse: @escaping (ItemWrapper) -> Void, onFailure: @escaping (String) -> Void) {
        
        ItemServices.getMoreitems(itemWrapper){ result in
        if let error = result.error {
            // TODO: improved error handling
            onFailure(error.localizedDescription)
        }
            onResponse(result.value!)
        }
    }

}
