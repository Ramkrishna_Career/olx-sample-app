import Foundation

public class TextRule: Rule {
    final public func evaluate(value: String?) throws {
        if let stringToEvaluate = value {
            try evaluateString(stringToEvaluate)
        } else {
            throw EvaluationError.failed("the value is nil")
        }
    }
    
    func evaluateString(_ stringToEvaluate: String) throws {
        throw EvaluationError.failed("validation has not been implemented")
    }
}

public class NotEmpty: TextRule {
    
    public override init() {
        
    }
    
    override func evaluateString(_ stringToEvaluate: String) throws {
        if stringToEvaluate.isEmpty  {
            throw EvaluationError.failed("the value is empty")
        }
    }
    
}

public class MinimumLength: TextRule {
    private var minLength: Int
    
    public init(_ minLength: Int) {
        self.minLength = minLength
    }
    
    override func evaluateString(_ stringToEvaluate: String) throws {
        if stringToEvaluate.characters.count < minLength  {
            throw EvaluationError.failed("less than \(minLength) characters")
        }
    }
    
}

public class MaximumLength: TextRule {
    private var maxLength: Int
    
    public init(_ maxLength: Int) {
        self.maxLength = maxLength
    }
    
    override func evaluateString(_ stringToEvaluate: String) throws {
        if stringToEvaluate.characters.count > maxLength  {
            throw EvaluationError.failed("more than \(maxLength) characters")
        }
    }
    
}
