This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* iPhone app that queries the public OLX API, parses the JSON-formatted response and displays the data.
* This is a Sample of my work , feel free to check it and find how i'm building iOS App.'
	


### Features List ###

* Domain driven and clean architecture.
* Use cases to load and save data from Local and Remote Datasource.
* Use Reachability and show error message if inernet connection lost.
* Support different screen sizes Using Auto Layout.
* Support Orientations [ Portrait , Landscape ] .
* Ready to use Mutable DataSources [Online and Offline]
* Paginated content
* Item detail page
* Use of custom protocol
* Supports TDD
* Caching Images and Saved into local core data.

### TO DO's ###

* Unit test
* Integration test
* UI Enhancement


### Navigation Pattern ###
* Created TabBar controller with Two Tabs.
* Search Tab to search the Item 
* History Tab which data is displayed from local synched database.
* Displaying detailed information related to Item.


### Language ###

* Swift 3.0
.
### Deployment instructions ###

* Check out the project with git
* Install the cocopods ( pod install)
* Run the project

### Dependencies ###

* Alamofire
* Validator Framework
* Reachability
* PKHUD
* RMessage
* Sync
   
    


![Simulator Screen Shot 22-Mar-2017, 6.49.54 PM.png](https://bitbucket.org/repo/RXB8Bz/images/289686688-Simulator%20Screen%20Shot%2022-Mar-2017,%206.49.54%20PM.png)